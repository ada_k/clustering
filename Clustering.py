#!/usr/bin/env python
# coding: utf-8

# # Cluster Analysis Course Notebook

# ### Importing Data files

# In[22]:


#Import Product DataSet here
import pandas as pd

product_data = pd.read_csv('Product Data Set - Student 2 of 3.csv', sep = '|')
product_data.head()


# In[24]:


#Import Transaction DataSet here

transactions_data = pd.read_csv('Transaction Data Set - Student 3 of 3.csv', sep = '|')
transactions_data.head()


# In[17]:


#Import Customer Dataset Here

customer_data = pd.read_csv('Customer Data Set - Student 1 of 3.csv')
customer_data.head()


# ### Changing data types

# In[15]:


customer_data['INCOME']=customer_data['INCOME'].map(lambda x : x.replace('$',''))


# In[14]:


customer_data['INCOME']=customer_data['INCOME'].map(lambda x : int(x.replace(',','')))


# ### Creating Customer View

# In[25]:


trans_products=transactions_data.merge(product_data,how='inner', left_on='PRODUCT NUM', right_on='PRODUCT CODE')
trans_products.head()


# In[26]:


trans_products['UNIT LIST PRICE']=trans_products['UNIT LIST PRICE'].map(lambda x : float(x.replace('$','')))


# In[27]:


trans_products['Total_Price']=trans_products['QUANTITY PURCHASED'] * trans_products['UNIT LIST PRICE'] * (1- trans_products['DISCOUNT TAKEN'])


# In[33]:


customer_prod_categ=trans_products.groupby(['CUSTOMER NUM','PRODUCT CATEGORY']).agg({'Total_Price':'sum'})
customer_prod_categ.head()


# In[34]:


customer_prod_categ=customer_prod_categ.reset_index()
customer_prod_categ.head()


# In[32]:


customer_pivot=customer_prod_categ.pivot(index='CUSTOMER NUM',columns='PRODUCT CATEGORY',values='Total_Price')
customer_pivot.head()


# In[35]:


trans_total_spend=trans_products.groupby('CUSTOMER NUM').agg({'Total_Price':'sum'}).rename(columns={'Total_Price':'TOTAL SPENT'})
trans_total_spend.head()


# In[36]:


customer_KPIs=customer_pivot.merge(trans_total_spend,how='inner',left_index=True, right_index=True )
customer_KPIs.head()


# In[37]:


customer_KPIs=customer_KPIs.fillna(0)
customer_KPIs.head()


# In[38]:


customer_all_view=customer_data.merge(customer_KPIs,how='inner', left_on='CUSTOMERID', right_index=True)


# In[39]:


customer_all_view.head()


# # Clustering  

# In[ ]:


#Begin Writing your code here


# In[44]:


from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering


# In[49]:


cluster_input = customer_all_view[['INCOME','TOTAL SPENT']]
cluster_input.head(5)


# In[47]:


Kmeans_model = KMeans(n_clusters=4)
Kmeans_model


# In[54]:


cluster_input['INCOME'] = cluster_input['INCOME'].map(lambda x: x.replace('$', ''))
cluster_input['INCOME'] = cluster_input['INCOME'].map(lambda x: float(x.replace(',', '')))


# In[53]:


cluster_input.dtypes


# In[56]:


cluster_output = Kmeans_model.fit_predict(cluster_input)
cluster_output


# In[57]:


type(cluster_output)


# In[65]:


product_data.head().values[1,:]
#product_data.head().values[1,2]


# In[68]:


#converting numpy array to pandas dataframe
cluster_output_pd=pd.DataFrame(cluster_output,columns=['segment'])
cluster_output_pd.head()


# In[71]:


segment_DF = pd.concat([cluster_input,cluster_output_pd], axis = 1)
segment_DF.head()


# In[75]:


Kmeans_model.cluster_centers_


# In[76]:


segment_DF[segment_DF.segment==0].head()
# segment_DF[segment_DF.segment==1].head()
# segment_DF[segment_DF.segment==2].head()
# segment_DF[segment_DF.segment==3].head()


# In[78]:


import matplotlib.pyplot as plt


# In[81]:


plt.scatter(segment_DF[segment_DF.segment==0]['INCOME'],segment_DF[segment_DF.segment==0]['TOTAL SPENT'],s=50, c='purple',label='Cluster1')

plt.scatter(segment_DF[segment_DF.segment==1]['INCOME'],segment_DF[segment_DF.segment==1]['TOTAL SPENT'],s=50, c='blue',label='Cluster2')

plt.scatter(segment_DF[segment_DF.segment==2]['INCOME'],segment_DF[segment_DF.segment==2]['TOTAL SPENT'],s=50, c='green',label='Cluster3')

plt.scatter(segment_DF[segment_DF.segment==3]['INCOME'],segment_DF[segment_DF.segment==3]['TOTAL SPENT'],s=50, c='cyan',label='Cluster4')

plt.scatter(Kmeans_model.cluster_centers_[:,0], Kmeans_model.cluster_centers_[:,1],s=200,marker='s', c='red', alpha=0.7, label='Centroids')

plt.title('Customer segments using K-means (k=4)')

plt.xlabel('Income')

plt.ylabel('Total Spend')

plt.legend()

plt.show()


# In[82]:


customer_demographics = pd.concat([customer_all_view, cluster_output_pd], axis=1)
customer_demographics.head()


# In[85]:


customer_demographics.groupby('segment').agg({'AGE':'mean', 'HOUSEHOLD SIZE':'median'})


# In[86]:


def percent_loyalty(series):
    percent=100 * series.value_counts()['enrolled'] /series.count()
    return percent

customer_demographics.groupby('segment').agg({'AGE':'mean','HOUSEHOLD SIZE':'median','LOYALTY GROUP': percent_loyalty})


# In[87]:


#agglomerative clustering
AgglomerativeClustering_model=AgglomerativeClustering(n_clusters=4)


# In[ ]:




